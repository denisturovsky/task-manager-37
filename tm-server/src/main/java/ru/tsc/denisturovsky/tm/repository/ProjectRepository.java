package ru.tsc.denisturovsky.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.DBConstants;
import ru.tsc.denisturovsky.tm.api.repository.IProjectRepository;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.model.Project;

import java.sql.*;
import java.util.Date;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project project) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_NAME, DBConstants.COLUMN_CREATED,
                DBConstants.COLUMN_DESCRIPTION, DBConstants.COLUMN_USER_ID, DBConstants.COLUMN_STATUS,
                DBConstants.COLUMN_DATE_BEGIN, DBConstants.COLUMN_DATE_END
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getName());
            statement.setTimestamp(3, new Timestamp(project.getCreated().getTime()));
            statement.setString(4, project.getDescription());
            statement.setString(5, project.getUserId());
            statement.setString(6, project.getStatus().toString());
            statement.setTimestamp(7,
                    project.getDateBegin() == null ? null : new Timestamp(project.getDateBegin().getTime())
            );
            statement.setTimestamp(8,
                    project.getDateEnd() == null ? null : new Timestamp(project.getDateEnd().getTime())
            );
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    public Project add(
            @NotNull final String userId,
            @NotNull final Project project
    ) throws Exception {
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws Exception {
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws Exception {
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        return add(project);
    }

    @NotNull
    @Override
    public Project fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(row.getString(DBConstants.COLUMN_ID));
        project.setName(row.getString(DBConstants.COLUMN_NAME));
        project.setCreated(row.getTimestamp(DBConstants.COLUMN_CREATED));
        project.setDescription(row.getString(DBConstants.COLUMN_DESCRIPTION));
        project.setUserId(row.getString(DBConstants.COLUMN_USER_ID));
        project.setStatus(Status.toStatus(row.getString(DBConstants.COLUMN_STATUS)));
        return project;
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConstants.TABLE_PROJECT;
    }

    @Override
    public void update(@NotNull final Project project) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_NAME, DBConstants.COLUMN_DESCRIPTION,
                DBConstants.COLUMN_STATUS, DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().toString());
            statement.setString(4, project.getId());
            statement.executeUpdate();
        }
    }

}
