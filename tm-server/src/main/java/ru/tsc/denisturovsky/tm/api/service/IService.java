package ru.tsc.denisturovsky.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.api.repository.IRepository;
import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    @NotNull
    List<M> findAll(@Nullable Sort sort) throws Exception;

    @Nullable
    M removeOneById(@Nullable String id) throws Exception;

    @Nullable
    M removeOneByIndex(@Nullable Integer index) throws Exception;

}
