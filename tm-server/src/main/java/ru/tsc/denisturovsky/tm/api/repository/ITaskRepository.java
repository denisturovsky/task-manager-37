package ru.tsc.denisturovsky.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.model.Task;

import java.sql.ResultSet;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task add(@NotNull Task task) throws Exception;

    @NotNull
    Task add(
            @NotNull String userId,
            @NotNull Task task
    ) throws Exception;

    @NotNull
    Task create(
            @NotNull String userId,
            @NotNull String name
    ) throws Exception;

    @NotNull
    Task create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws Exception;

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    ) throws Exception;

    @NotNull
    Task fetch(@NotNull ResultSet row) throws Exception;

    void update(@NotNull Task task) throws Exception;

}
